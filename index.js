const _ = require('lodash');

/**
 * Execute the specified function at an
 * arbitrary time 100ms to 1second
 */
function async(fn, start = 1000, end = 5000) {
  _.delay(fn, _.random(start, end));
}

/**
 * Create a promise which resolves only 
 * when the passed in function evaluates as true
 */
function waitUntil(fn, waitRetryMs = 200) {
  return new Promise(resolve => {
    
    function evaluate(resolve) {
      if (fn() === true) {
        resolve();
      } else {
        setTimeout(() => evaluate(resolve));
      }
    }

    evaluate(resolve);
  });
}

const state = {
  numCustomers: 50,
  barber: null,
  waiting: [],
  completed: [],
  inProgress: null,
  //Create the states for when a customer or the barber is in the process of checking
  customercheck: null,
  barbercheck: null
};

class Barber {

  constructor() {
    this.sleeping = false;
    this.name = _.uniqueId('barber-');

    this.checkCustomers();
  }

  sleep() {
    //the barber is not checking if sleeping
    state.barbercheck = null;
    console.info(`${this.name} is going to sleep`);
    this.sleeping = true;
  }

  awaken() {
    console.info(`${this.name} woke up`);
    this.sleeping = false;
  }

  checkCustomers() {
    //barber is now checking the waiting line
    state.barbercheck = true;
    console.info(`${this.name} is going to go check on customers`);

    async(() => {
      if (state.waiting.length > 0) {
        this.cutHair(state.waiting.pop());
      } else {
        this.sleep();
      }
    });
  }

  cutHair(customerName) {
    // remove them from the line
    //^the pop() function already removes them
    state.inProgress = customerName;
    //both the customer and barber are not checking if a haircut is being given
    state.barbercheck = null;
    state.check = null;
    //state.waiting = _.remove(state.waiting, customerName);
    //^no longer need this statement
    
    console.info(`${this.name} is cutting ${customerName}s hair`);

    async(() => {
      console.info(`${this.name} finished cutting ${customerName}s hair`);

      state.inProgress = null;
      state.completed.unshift(customerName);
      this.checkCustomers();  
    });
  }
}

class Customer {

  constructor() {
    this.waiting = false;
    this.name = _.uniqueId('constumer-');

    this.check();
  }

  check() {
  //Check the waiting list isn't empty,
  //there isn't someone already checking the barber,
  //and the barber isnt checking the waiting line
    if (state.waiting.length > 0 || state.customercheck == true || state.barbercheck == true) {
      console.info(`${this.name} is waiting.`);
      state.waiting.unshift(this.name);
    } else {
      //customer is now checking the barber 
      state.customercheck = true;
      this.checkBarber();
    }
  }

  checkBarber() {
    console.info(`${this.name} is checking the barber`);

    async(() => {
      // barber is free
      if (state.inProgress == null) {
        if (state.barber.sleeping) {
          console.info(`${this.name} is awakening the barber.`);
          state.barber.awaken();
        }

        state.barber.cutHair(this.name);  
      } else {
        console.info(`The barber is busy ${this.name} is going back to wait`);

        async(() => {
          state.waiting.unshift(this.name);
        });
      }
    });
  }

}

(function main() {
  var customers = [];

  state.barber = new Barber();
  for (let i = 0, end = state.numCustomers; i < end; i++) {
    async(() => {
      customers.push(new Customer());
    });
  }

  process.on('exit', function () {
    console.info(`Completed haircuts for ${state.completed.length} of ${state.numCustomers} customers`);
  });

}());

